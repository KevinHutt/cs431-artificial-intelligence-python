Kevin Hutt
CS 431
Written Responses for Lab2

1a. Uninformed breadth first search would not work on this problem because
	the worst case time and space complexity are exponential and there are way
	too many possible states to explore them all in a resonable amount of time
	
1b. Uninformed recursive backtracking would not be a good solution. Since the 
	search is uninformed, the algorithm would just pick a branch at random and
	the odds of picking a good branch is really low. Also, the algorithm has
	travel all the way down the tree to a schedule before it will know if
	the schedule is good or not. Then with backtracking it will just go up
	the tree a little ways then back down another nearby branch, most likely
	finding another bad schedule. 