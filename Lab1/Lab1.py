# -*- coding: utf-8 -*-
"""
Created on Tue Jan 10 16:15:58 2017

@author: Kevin Hutt
@description: CS 431 Lab1, Minesweeper
@notes: Prints out first 2 solutions found with B for a bomb space and
        C for a clear space. I know there is a lot I could do optimize this,
        right now it runs REALLY slow and I think it gets stuck on the
        larger boards.
"""

import numpy as np
np.set_printoptions(threshold=np.inf, linewidth=np.inf)


def main(filename):

    # read in the board and mark any spaces known to be bombs or not-bombs
    arr = mark(readBoard(filename))

    if solved(arr):
        print('only one solution')
        printBoard(arr)

    if isValid(arr):
        solve(arr, [])
    else:
        print('Invalid board')


# try to solve the board with recursive backtracking, keep a list of solutions
def solve(array, solutions):

    # if the array is a new solution, add it to the list
    if solved(array) and not (any((array == x).all() for x in solutions)):
        solutions.append(array)

        if len(solutions) > 1:
            print("More than one solution found: ")
            for array in solutions:
                printBoard(array)
            return True

    # check each adjacent state for a solution
    for adj in adjacent(array):
        if solve(adj, solutions):
            return True


# get a list of adjacent boards, i.e. turning one unkown into a bomb
def adjacent(array):
    adj = []

    # get unknown cells
    (ur, uc) = np.where(array == -1)
    ur = ur.tolist()
    uc = uc.tolist()

    # for each unknown, switch it to a bomb and add the new board to adj list
    for index, int in enumerate(ur):
        new = array.copy()
        new[ur[index], uc[index]] = -2
        new = mark(new)
        if isValid(new):
            adj.append(new)

    return adj


# check if the board is a solution
def solved(array):
    # surround the array with zeroes
    pad = np.pad(array, ((1, 1), (1, 1)), mode='constant', constant_values=0)

    # get known cells
    (kr, kc) = np.where(array > -1)
    kr = kr.tolist()
    kc = kc.tolist()

    # iterate through known cells, check if solved
    for index, int in enumerate(kr):
        rindex = kr[index] + 1
        cindex = kc[index] + 1

        knowns = pad[rindex - 1:rindex + 2, cindex - 1: cindex + 2]

        bombs = (knowns == -2).sum()

        if (pad[rindex, cindex] != bombs):
            return False

    return True


# mark all locations that have to be bombs or clear cells
def mark(array):

    # surround the array with zeroes
    pad = np.pad(array, ((1, 1), (1, 1)), mode='constant', constant_values=0)

    # get known cells
    (kr, kc) = np.where(array > -1)
    kr = kr.tolist()
    kc = kc.tolist()

    # iterate through known cells, marking known bomb locations as -2
    for index, int in enumerate(kr):
        rindex = kr[index] + 1
        cindex = kc[index] + 1

        knowns = pad[rindex - 1:rindex + 2, cindex - 1:cindex + 2]

        ukns = (knowns == -1).sum()
        bombs = (knowns == -2).sum()

        if pad[rindex, cindex] == (ukns + bombs):
            knowns[knowns == -1] = -2

    # iterate through known cells, marking all known clear locations as -3
    for index, int in enumerate(kr):
        rindex = kr[index] + 1
        cindex = kc[index] + 1

        knowns = pad[rindex - 1:rindex + 2, cindex - 1:cindex + 2]

        ukns = (knowns == -1).sum()
        bombs = (knowns == -2).sum()

        if pad[rindex, cindex] == bombs:
            knowns[knowns == -1] = -3

    # if the array has been changed, recursively call mark(array)
    if not np.array_equal(array, pad[1:-1, 1:-1]):
        array = pad[1:-1, 1:-1]
        mark(array)

    return array


# check if the board is valid
def isValid(array):

    # surround the array with zeroes
    pad = np.pad(array, ((1, 1), (1, 1)), mode='constant', constant_values=0)

    # get known cells
    (kr, kc) = np.where(array > -1)
    kr = kr.tolist()
    kc = kc.tolist()

    # iterate through known cells, check if valid
    for index, int in enumerate(kr):
        rindex = kr[index] + 1
        cindex = kc[index] + 1

        knowns = pad[rindex - 1:rindex + 2, cindex - 1:cindex + 2]

        ukns = (knowns == - 1).sum()
        bombs = (knowns == - 2).sum()

        if (pad[rindex, cindex] > (ukns + bombs)) or (
                bombs > pad[rindex, cindex]):
                return False

    return True


# print out the board, replace -2 with B for bomb and -3 with C for clear
def printBoard(array):
    array = np.char.mod('%d', array)
    print(str(array).replace("'", '').replace('-2', 'B').replace('-3', 'C'))


# read in the board as a numpy array of ints
def readBoard(filename):

    b = open(filename)
    str = b.read()
    b.close()

    if '-1' not in str:
        str = str.replace('-', '-1')

    l = str.split()
    l = [int(i) for i in l]
    r = l.pop(0)
    c = l.pop(0)
    arr = np.asarray(l).reshape(r, c)

    print(filename, end=': \n')
    return arr
